### 开源微商城，可供商用或学习

### 技术架构

项目使用spring boot

数据持久层 hibernate

安全框架 shiro

连接池 druid

前端 vue route weui jquery bootstrap



### 使用说明


本项目使用vue分离了前后端，并没有使用模版引擎，运行项目以后，打开静态文件即可

前端链接 http://localhost:8080/home/index.html

后台链接 http://localhost:8080/admin/index.html

### 效果图


![输入图片说明](https://git.oschina.net/uploads/images/2017/0826/143300_8be80f27_420218.png "1.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0826/143319_a0cba742_420218.png "2.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0826/143338_83f732bb_420218.png "3.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0826/143358_ddfb75c5_420218.png "4.png")